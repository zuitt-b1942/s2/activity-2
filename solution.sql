
--CREATE DATABASE
	CREATE DATABASE blog_db;

--CREATE "users" TABLE
	CREATE TABLE users(
		id INT NOT NULL AUTO_INCREMENT,
		email VARCHAR(100) NOT NULL,
		password VARCHAR(300) NOT NULL,
		datetime_created DATETIME NOT NULL,
		PRIMARY KEY (id)
	);

--CREATE "posts" TABLE
	CREATE TABLE posts(
		id INT NOT NULL AUTO_INCREMENT,
		title VARCHAR(500) NOT NULL,
		content VARCHAR(5000) NOT NULL,
		author_id INT NOT NULL,
		user_id INT NOT NULL,
		datetime_posted DATETIME NOT NULL,
		PRIMARY KEY (id),
		CONSTRAINT KEY fk_posts_user_id
			FOREIGN KEY(user_id) REFERENCES users(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
	);

--CREATE "posts_comments" TABLE
	CREATE TABLE posts_comments(
		id INT NOT NULL AUTO_INCREMENT,
		content VARCHAR(5000) NOT NULL,
		post_id INT NOT NULL,
		user_id INT NOT NULL,
		datetime_commented DATETIME NOT NULL,
		PRIMARY KEY (id),
		CONSTRAINT KEY fk_posts_comments_user_id
			FOREIGN KEY(user_id) REFERENCES users(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,
		CONSTRAINT KEY fk_posts_comments_post_id
			FOREIGN KEY(post_id) REFERENCES post(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
	);

--CREATE "posts_likes" TABLE	
	CREATE TABLE posts_likes(
		id INT NOT NULL AUTO_INCREMENT,
		post_id INT NOT NULL,
		user_id INT NOT NULL,
		datetime_liked DATETIME NOT NULL,
		CONSTRAINT KEY fk_posts_likes_user_id
			FOREIGN KEY(user_id) REFERENCES users(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,
		CONSTRAINT KEY fk_posts_likes_post_id
			FOREIGN KEY(post_id) REFERENCES post(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
	);